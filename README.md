Repository for CMS GWMS frontend configurations management.

There are two main branches that correspond to respective frontends:
- CERN
- FNAL

The respective frontend configurations are placed in corresponding branches.
Each branch is further divided into directories based on pools.
